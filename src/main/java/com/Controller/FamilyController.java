package com.Controller;

import com.DAO.FamilyDao;
import com.Family;
import com.Human;
import com.Pet;
import com.Service.FamilyService;

import java.util.ArrayList;
import java.util.List;

public class FamilyController  implements FamilyDao {
    FamilyService familyService=new FamilyService();


    @Override
    public List getAllFamilies() {
        return familyService.getAllFamilies();
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }





        @Override
    public void displayAllFamilies() {
         familyService.displayAllFamilies();
    }

    @Override
    public void getFamiliesBiggerThan(int number) {
          familyService.getFamiliesBiggerThan(number);
    }

    @Override
    public void getFamiliesLessThan(int number) {
          familyService.getFamiliesLessThan(number);
    }

    @Override
    public void createNewFamily(Human human, Human human2) {
          familyService.createNewFamily(human,human2);

    }

    @Override
    public boolean deleteFamily(int index) {
        return familyService.deleteFamily(index );

    }

    @Override
    public long count() {
          return familyService.count();
    }

    @Override
    public boolean deleteFamily(Family family) {
                 return familyService.deleteFamily(family );
     }

    @Override
    public boolean saveFamily(List<Family> family) {
        return familyService.saveFamily(family );
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
          return familyService.deleteFamilyByIndex(index);
    }

    @Override
    public void bornChild(Family family, ArrayList<Human> name) {
//          familyService.bornChild(family,name);
    }

    @Override
    public Family getFamilyById(int number) {
         return familyService.getFamilyById(number);
    }

    @Override
    public List getPets(int number) {
        return familyService.getPets(number);
    }

    @Override
    public void addPet(int index, Pet pet) {
          familyService.addPet(index,pet);

    }

}

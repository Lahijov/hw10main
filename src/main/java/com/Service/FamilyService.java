package com.Service;

import com.DAO.CollectionFamilyDao;
import com.DAO.FamilyDao;
import com.Family;
import com.Human;
import com.Pet;

import java.util.ArrayList;
import java.util.List;

public class FamilyService implements FamilyDao {
//    private static FamilyService familyServices=new FamilyService();

    private static FamilyDao collectionFamilyDao=new CollectionFamilyDao();


    @Override
    public Family getFamilyByIndex(int index) {
        return collectionFamilyDao.getFamilyByIndex(index);
    }

    @Override
    public List getAllFamilies() {
        return collectionFamilyDao.getAllFamilies();
    }



    @Override
    public void displayAllFamilies() {
        collectionFamilyDao.displayAllFamilies();
    }

    @Override
    public void getFamiliesBiggerThan(int number) {
        collectionFamilyDao.getFamiliesBiggerThan(number);
    }

    @Override
    public void getFamiliesLessThan(int number) {
        collectionFamilyDao.getFamiliesLessThan(number);
    }

    @Override
    public void createNewFamily(Human human, Human human2) {
        collectionFamilyDao.createNewFamily(human,human2);

    }

    @Override
    public boolean deleteFamily(int index) {
        return collectionFamilyDao.deleteFamily(index);
    }

    @Override
    public long count() {
         return collectionFamilyDao.count();
    }

    @Override
    public boolean deleteFamily(Family family) {
        return collectionFamilyDao.deleteFamily(family);
    }

    @Override
    public boolean saveFamily(List<Family> family) {
        return collectionFamilyDao.saveFamily(family);


    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        return collectionFamilyDao.deleteFamilyByIndex(index);
    }

    @Override
    public void bornChild(Family family, ArrayList<Human> name) {

    }


    @Override
    public Family getFamilyById(int number) {
        return collectionFamilyDao.getFamilyById(number);
    }

    @Override
    public List getPets(int number) {
        return collectionFamilyDao.getPets(number);
    }

    @Override
    public void addPet(int index, Pet pet) {
        collectionFamilyDao.addPet(index,pet);

    }
}

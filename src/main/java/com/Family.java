package com;


import java.util.ArrayList;

public class Family {
    private Human mother;
    Human father;
    List<Human> children = new ArrayList<>();
    Pet pet;

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

   
    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }


    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }

     public Family(Human mother, Human father, List<Human> children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }


    public int sizeBefore;

    public void addChild(Human human) {
        int i;
        int n = children.size();
        sizeBefore = countFamily();
        ArrayList<Human> newChild = new ArrayList<>(n + 1);
        for (i = 0; i < n; i++)
            newChild.add(children.get(i));
        newChild.add(newChild.size(), human);
        this.children = newChild;
        sizeAfter = countFamily();
    }

    public boolean deleteChild(int index) {
        boolean deleted = false;
        ArrayList<Human> child = new ArrayList<>(children.size() - 1);

        int i = 0;
        try {
            for (int j = 0; j < children.size(); j++) {
                if (j != index) {
                    child.add(i++, children.get(j));
                }
            }
            if (index < children.size()) {
                deleted = true;
            }
            this.children = child;

            return deleted;
        } catch (
                Exception exception) {

            System.out.println("error");
            return false;
        }

    }

    public int sizeAfter;

    public int countFamily() {

        return 2 + children.size();
    }

}


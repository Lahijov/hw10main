
package com;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Human {
    String name;
    String surname;
    long birthDate;
    int iq;
    Family family;
    Map<String, Integer> schedule = new HashMap<>();

    public void welcome() {
        System.out.println("Hello," + family.getPet().nickname);
    }

    public String describeAge() {
        Date date = new Date ();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        date.setTime((long)birthDate*1000);
        String dateText = df.format(date.getTime());


        return dateText;
    }




    public void favouritePet() {
        System.out.print("I have a " + family.getPet().getSpecies() + "," + "he is " + family.getPet().age +
                " years old,he is ");
        if (iq < 50) {
            System.out.println("almost not sly");
        } else if (iq > 50) {
            System.out.println("very sly");
        }
    }

    public void feed() {
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birtdate=" + describeAge() +
                '}';
    }

    public Human(String name, String surname, long birthDate,int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq=iq;
    }



    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;

    }


    public Human() {
    }

    public Human(String name, String surname, long birthDate, int iq, Map schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate =   birthDate;
        if (iq <= 100 || iq >= 1) {
            this.iq = iq;
        } else {
            System.out.println("Your number is higher or smaller than 100 and 1,respectively.So,iq level is set 99 by system");
            this.iq = 99;
        }

        this.schedule = schedule;
    }
}


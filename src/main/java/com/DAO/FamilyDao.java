package com.DAO;

import com.Family;
import com.Human;
import com.Pet;

import java.util.ArrayList;
import java.util.List;

public interface FamilyDao {
    Family getFamilyByIndex(int index);

    List getAllFamilies();

    void displayAllFamilies();

    void getFamiliesBiggerThan(int number);

    void getFamiliesLessThan(int number);

    void createNewFamily(Human human, Human human2);

    boolean deleteFamily(int index);

    public long count();

    boolean deleteFamily(Family family);

    boolean saveFamily(List<Family> family);

    public boolean deleteFamilyByIndex(int index);

    public void bornChild(Family family, ArrayList<Human> name);
 
    public Family getFamilyById(int number);

    public List getPets(int number);

    public void addPet(int index, Pet pet);


}

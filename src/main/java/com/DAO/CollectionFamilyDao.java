package com.DAO;

import com.Family;
import com.Human;
import com.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CollectionFamilyDao implements FamilyDao {


    private List<Family> databaseFamily = new ArrayList<>();


    @Override
    public Family getFamilyByIndex(int index) {
        try {
            return databaseFamily.get(index);

        } catch (Exception ex) {

            return null;
        }


    }

    @Override
    public boolean deleteFamily(int index) {
        try {
            databaseFamily.remove(index);
            return true;
        } catch (Exception e) {
            System.out.println("There is not any family in such index");
            return false;
        }


    }

    @Override
    public boolean deleteFamily(Family family) {
        try {
            databaseFamily.remove(family);
            return true;
        } catch (Exception e) {
            System.out.println("There is not any family");
            return false;
        }


    }

    @Override
    public boolean saveFamily(List<Family> family) {
        try {
            this.databaseFamily = family;
            return true;

        } catch (IllegalAccessError error) {
            error.printStackTrace();
            return false;
        }

    }


    @Override
    public List getAllFamilies() {
        ArrayList<Family> families = new ArrayList<>();
        for (Family fam : families
        ) {
            families.add(fam);

        }
        ;

        return families;
    }

    @Override
    public void displayAllFamilies() {
        System.out.println(getAllFamilies());
    }

    @Override
    public void getFamiliesBiggerThan(int number) {
        ArrayList<Family> families = new ArrayList<>();
        for (Family fam : families) {
            if (fam.countFamily() > number) {
                families.add(fam);
            }

        }
        System.out.println(families);
    }

    @Override
    public void getFamiliesLessThan(int number) {
        ArrayList<Family> families = new ArrayList<>();
        for (Family fam : families) {
            if (fam.countFamily() < number) {
                families.add(fam);
            }

        }
        System.out.println(families);
    }

    @Override
    public void createNewFamily(Human human, Human human2) {


        Family family1 = new Family(human, human2);
        databaseFamily.add(family1);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Your parametr have been saved in database,do you want to create an other family?");
        String condition = scanner.nextLine();
        int count = 0;
        if (condition.toLowerCase().equals("yes")) {

            System.out.println("Enter the name and surname of mother");
            Scanner scannerm = new Scanner(System.in);
            String name_m = scannerm.nextLine();

            Scanner scanne = new Scanner(System.in);
            String surname_m = scanne.nextLine();

            System.out.println("Enter the name and surname of father");


            Scanner scannerf = new Scanner(System.in);
            String name_f = scannerf.nextLine();


            Scanner scan = new Scanner(System.in);
            String surname_f = scan.nextLine();

            Family family = new Family(new Human(name_m, surname_m), new Human(name_f, surname_f));
            databaseFamily.add(family);

        }

        System.out.println("Thank you!");
    }

    @Override
    public long count() {
        return databaseFamily.size();
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        try {
            databaseFamily.remove(index);
            return true;
        } catch (Exception exception) {
            System.out.println("Something went wrong");
            return false;
        }

    }

    @Override
    public void bornChild(Family family, ArrayList<Human> name) {
        family.setChildren(name);

    }

    @Override
    public Family getFamilyById(int number) {
        return databaseFamily.get(number);

    }

    @Override
    public List getPets(int number) {
        ArrayList<Pet> List = new ArrayList<>();
        List.add(databaseFamily.get(number).getPet());

        return List;

    }

    @Override
    public void addPet(int index, Pet pet) {
        try {
            databaseFamily.get(index).setPet(pet);
            System.out.println("Uptaded");
        } catch (Exception ex) {
            System.out.println("Something went wrong!");
            throw ex;
        }

    }


}

